## Bash script that converts a Markdown file to a ready to use Reveal.js presentation.

This script takes as input a Markdown file and outputs an HTML file that contains all needed Reveal.js JavaScript and CSS, included via CDN. 
The Reveal.js presentation will be ready to use, you will only need to provide a background image (optional).

The only dependencies are Bash, Sed, Grep, and Cut (all are available in most Unix-like systems).

Credit for the Markdown to HTML conversion code : https://github.com/chadbraunduin/markdown.bash

#### Available options :

- Background image
- Theme (white/league/beige/sky/nigth/serif/simple/solarized/blood/moon) default : SIMPLE
- Transition (none/fade/slide/convex/concave/zoom) default : SLIDE

Use Horizontal Rules ("---" with a leading and following blank line) to delimitate slides.

Example:

```
## This is the first slide

First slide text

---

## This is the second slide

Second slide text
```

Usage : 

```
sudo chmod +x blurt.sh

./blurt.sh -i file.md -o file.html
```